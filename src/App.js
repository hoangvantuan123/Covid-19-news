import React from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'
import CoronaVirus from './Components/pages/CoronaVirus';
import Discoveries from './Components/pages/Discoveries';
import Health from './Components/pages/Health';
import Vaccines from './Components/pages/Vaccines';
function App() {
    return (
        <div className="App  bg-[#F1F1F2] ">
            <Router>
                <Routes>
                    <Route exact path='/' element={<Discoveries />} />
                    <Route exact path='/CoronaVirus' element={<CoronaVirus />} />
                    <Route exact path='/Vaccines' element={<Vaccines />} />
                    <Route exact path='/Health' element={<Health />} />
                </Routes>
            </Router>
        </div>
    );
}

export default App;
