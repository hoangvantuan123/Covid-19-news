
import React from 'react';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
    

export default function CountrieSelector({ countries, handleOnChange, value }) {


    return (
        <FormControl sx={{ m: 1, width: 300 }}>

            <Select
                value={value}
                labelId="demo-multiple-name-label"
                id="demo-multiple-name"
                onChange={handleOnChange}
                inputProps={{
                    name: 'country',
                    id: 'country-selector',
                }}
            >
                {countries.map(({ name , code}) => (

                    <MenuItem variant="outlined" key={name} value={code} label={name} >{name}</MenuItem>
                ))}


            </Select>

            {/*  <select
                value={value}
                onChange={handleOnChange}
            >
                {countries.map(({ Country, ISO2 }) => (
                    <option key={ISO2} value={ISO2.toLowerCase()} >
                        {Country}
                    </option>
                ))}
            </select> */}

        </FormControl>
    );
}

CountrieSelector.defaultProps = {
    countries: [],
};