import React, { useEffect } from 'react';
import { sortBy } from 'lodash';
import { getCountries2, getReportByCountry2, runAPICountry } from '../API';
import CountrieSelector from './CountrieSelector';
import Charts from '../Charts';
export default function CountriesUI() {
    const [countries, setCountries] = React.useState([]);
    const [selectedCountryId, setSelectedCountryId] = React.useState('');
    const [report, setReport] = React.useState([]);

    useEffect(() => {
        getCountries2().then((res) => {
            console.log({ res })
            const { data } = res;
            const countries = sortBy(data, 'name');
            setCountries(countries);
            setSelectedCountryId('VNM');

        });
    }, []);



    const handleOnChange = React.useCallback((e) => {
        setSelectedCountryId(e.target.value);
    }, []);


    /*  console.log('tiel',selectedCountryId);  */

    useEffect(() => {
        if (selectedCountryId) {
            const selectedCountry = countries.find(
                (name) => name.code === selectedCountryId.toUpperCase()
            );
            console.log('test1', selectedCountry);
            getReportByCountry2(selectedCountry.code).then((res) => {

                // remove last item = current date
                res.data.pop();
                setReport(res.data);
            });

        }
    }, [selectedCountryId, countries]);
    console.log('1',selectedCountryId)

    return (
        <div>
            <CountrieSelector
                handleOnChange={handleOnChange}
                countries={countries}
                value={selectedCountryId}
            />
            <Charts report={report} />
        </div>
    )
}
