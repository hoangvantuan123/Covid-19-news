import React from 'react'

export default function Header() {
    return (
        <div className=''>
            <div className="title uppercase pt-4 font-inter font-bold text-9xl text-tahiti-100 ">
                <div>
                    coronavirus
                </div>
                <div >
                    News <span className=' font-inria'>V.1</span>
                </div>
            </div>
            <p className=' font-inria text-xl font-normal  text-tahiti-100 max-w-[350px] m-auto  pt-10 text-right'>Our goal is to make data and research accessible so we can make progress on the world's biggest problems.</p>
        </div>
    )
}
