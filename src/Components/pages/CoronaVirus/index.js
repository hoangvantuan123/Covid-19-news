import React from 'react'
import Header from './header'
import BackgroundImage from '../../file/Images/khang-the-troi-cho-trungquoc-omicron-2022-02-04-15-05.jpg'
import CountriesUI from '../../countriesUI'

export default function CoronaVirus() {
  return (
    <>
      <div className='max-w-[1250px]  m-auto justify-center items-center'>
        <Header />
      </div>
      <div className=' w-[100%] '>
        <img src={BackgroundImage} alt="" className=' w-[100%] h-[640px] object-cover' />
      </div>
      <div className='max-w-[1250px]  m-auto justify-center items-center'>
        <div>

          <h2 className=' font-inria font-semibold text-4xl text-[#3E3E3E] capitalize' >
            Explore global data on <br />
            Infection and death from Covid-19
          </h2>
          <p className=' font-inria text-xl font-normal  text-tahiti-100 max-w-[550px] m-auto  pt-10 '>This page has a large number of charts on the pandemic. In the box below you can select any country you are interested in – or several, if you want to compare countries.
            <br />
            <br />
            All charts on this page will then show data for the countries that you selected.</p>
        </div>
       
        <CountriesUI/>
      </div>
    </>
  )
}
