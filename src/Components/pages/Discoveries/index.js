import React from 'react'
import Country from '../../Country'
import News from '../../news'
import World from '../../world'
import Disease from './disease'
import Focus from './focus'
import FooterPage from './footer'
import Title from './header/Header'
import Reports from './reports'


export default function Discoveries() {
  return (
    <div className=" bg-[#F1F1F2]">
      <div className='max-w-[1250px]  m-auto justify-center items-center'>
        <Title />
        <World />
        <Country />
        <Reports />
        <Disease />
        <Focus />
        <News/>
      </div>
        <FooterPage/>
    </div>
  )
}
