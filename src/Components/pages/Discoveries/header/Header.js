import React from 'react'

export default function Title() {
    return (
        <section className=' mb-24 min-h-[1000px]'>
            <div className="title uppercase pt-4 font-inter font-bold text-9xl text-tahiti-100 ">
                <div>
                    discoveries
                </div>
                <div className="  ml-[568px] ">
                    Covid-19
                </div>
            </div>
            <div className="silde  pt-24">
                <div className=' w-[1250px] h-[646px] bg-[#E9E8EE] rounded-3xl  absolute '>
                    <div className=" w-[1250px] h-[646px]  bg-[#B6CAE0] blur-xl items-center justify-center m-auto  absolute z-0 ">
                    </div>
                    <div className='  absolute z-20  top-1/2 left-1/2  translate-x-[-50%] translate-y-[-50%] m-auto justify-center items-center font-inter font-bold' >
                        <div className="  text-7xl text-[#3E3E3E] w-[1000px] " >
                            <h1 >
                                Coronavirus Disease
                            </h1>
                            <h1 className=' ml-[170px]'>
                                (COVID-19) Pandemic
                            </h1>
                        </div>
                        <br />
                        <p className=' relative w-[400px] text-right text-base text-[#3C4E66] ml-[540px]'>
                            This website continuously updates the situation of the disease caused by a coronavirus (COVID-19)
                            with an overview of global COVID-19 cases and deaths, regional and national cables, highlighting important data and trends
                        </p>
                    </div>
                </div>
            </div>
            
        </section>
    )
}
