import React from 'react'
import Image1 from '../../../file/Images/khang-the-troi-cho-trungquoc-omicron-2022-02-04-15-05.jpg'
import Image2 from '../../../file/Images/Image2.png'
import Image3 from '../../../file/Images/Image3.png'
import { Link } from 'react-router-dom'


export default function Focus() {
    return (
        <div>
            <h2 className=' font-inria font-semibold text-4xl text-[#3E3E3E]' >
                Area Of Our Focus
            </h2>
            <p className=' font-inria text-lg font-normal leading-6 '>
                Explore the volatility of the COVID-19 pandemic
                through indicators and quick reports with the
                topics we collect.
            </p>
            <div className=' flex gap-16  justify-center mt-24'>
                <div className=' relative  text-center w-[340px] h-[340px] '>
                    <Link to='/CoronaVirus'>
                        <img src={Image1} alt="" className='rounded-full  object-cover w-[340px] h-[340px]  ' />
                        <h3 className=' absolute top-[50%] left-[50%]  text-center translate-x-[-50%] translate-y-[-50%] uppercase font-inter font-bold text-5xl text-[#FFFFFF]'>
                            corona
                            virus
                        </h3>
                    </Link>
                </div>
                <div className=' relative  text-center w-[340px] h-[340px] '>
                    <Link to='/Vaccines'>
                        <img src={Image2} alt="" className='rounded-full  object-cover w-[340px] h-[340px]  ' />
                        <h3 className=' absolute top-[50%] left-[50%]  text-center translate-x-[-50%] translate-y-[-50%] uppercase font-inter font-bold text-5xl text-[#FFFFFF]'>
                            vaccines
                        </h3>
                    </Link>
                </div>
                <div className=' relative  text-center w-[340px] h-[340px] '>
                    <Link to='/Health'>
                        <img src={Image3} alt="" className='rounded-full  object-cover w-[340px] h-[340px]  ' />
                        <h3 className=' absolute top-[50%] left-[50%]  text-center translate-x-[-50%] translate-y-[-50%] uppercase font-inter font-bold text-5xl text-[#FFFFFF]'>
                            health
                        </h3>
                    </Link>
                </div>

            </div>

        </div>
    )
}
