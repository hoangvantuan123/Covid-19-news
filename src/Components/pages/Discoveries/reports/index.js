import React from 'react'
import { Link } from 'react-router-dom'
import Arrow from '../../../file/svg/Arrow.svg'


export default function Reports() {
    return (
        <div>
            <h2 className=' font-inria font-semibold text-4xl text-[#3E3E3E]' >
                COVID-19 Data And Reports
            </h2>
            <div className='text-[#3E3E3E]'>
                <p className=' font-inria text-lg font-normal leading-6 '>
                    In a fast-evolving pandemic it is not a simple matter to identify the countries that are most successful in making progress against it. For a comprehensive assessment,
                    we track the impact of the pandemic across our publication and we built country profiles for 207 countries to study in depth the statistics on the coronavirus pandemic for every country in the world.

                    <br />
                    Each profile includes interactive visualizations, explanations of the presented metrics, and the details on the sources of the data.
                </p>
                <div className=' mt-4' >
                        <Link to="/CoronaVirus">
                            <span className=' font-inter font-semibold text-lg flex  '>
                                SEE MORE
                                <img src={Arrow} alt="" className=' ml-6' />
                            </span>
                        </Link>
                </div>
            </div>
        </div>
    )
}
