import React, { useEffect, useState } from 'react'
import ReactEcharts from "echarts-for-react";
import moment from 'moment';

function Charts({ report }) {
    const generateOptions = (report) => {

        const categories = report.map((item) => moment(item.Date).format('MM/YYYY'));
        return {
            tooltip: {
                trigger: 'axis',
                position: function (pt) {
                    return [pt[0], '10%'];
                }
            },
            xAxis: {
                type: 'category',
                boundaryGap: false,
                data: categories

            },
            yAxis: {
                type: 'value',
                boundaryGap: [0, '100%']
            },
            dataZoom: [
                {
                    type: 'inside',
                    start: 0,
                    end: 100
                },
                {
                    start: 0,
                    end: 100
                }
            ],


            series: [
                {
                    data: report.map((item) => item.deaths),
                    type: 'line',
                    symbol: 'none',
                    sampling: 'lttb',
                    itemStyle: {
                        color: 'rgb(255, 70, 131)'
                    },
                    

                },
                

            ],

        }
    }
    const [options, setOptions] = useState({});

    useEffect(() => {
        setOptions(generateOptions(report));
    }, [report]);



    return (
        <div>
            <ReactEcharts option={(options)} data={report} ></ReactEcharts>
        </div>
    )
}

export default Charts
