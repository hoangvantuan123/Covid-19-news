import React, { useEffect } from 'react'
import Map from '../file/svg/WorldMap.svg'
import { getWorldData } from '../API';
import { sortBy } from 'lodash';
import { Box } from '@mui/material';



export default function World() {

    const [world, setWorld] = React.useState([]);

    // World Data API 
    useEffect(() => {
        getWorldData().then((response) => {
            /*  console.log({ response }) ; */
            const { data } = response;
            const world = sortBy(data);
            setWorld(world);
        });
    }, []);


    return (
        <section className=' min-h-[800px]  m-auto text-right  '>
            <div className=' mb-6'>
                <h2 className=' font-inria font-semibold text-4xl text-[#3E3E3E]' >
                    All Over The World
                </h2>
            </div>
            <div className=' font-inter w-full h-full'>
                <img src={Map} alt="WorldMap" className='m-auto float-right ' />
                <Box>
                    <div className=' bg-[#E9E8EE] w-[310px] h-[130px] relative rounded-[20px]'>
                        <div className=' absolute top-[50%] left-[50%] translate-x-[-50%] translate-y-[-50%] w-[260px] text-center'>
                            Total Number Of Infections
                            <h5>
                                {
                                    world.map(item => {
                                        return <span key={item}  className='text-[#286DA8] text-4xl'>
                                            {item.TotalCases}
                                        </span>
                                    })
                                }
                            </h5>
                        </div>
                    </div>
                    <div className=' bg-[#E9E8EE] w-[310px] h-[130px] relative rounded-[20px]'>
                        <div className=' absolute top-[50%] left-[50%] translate-x-[-50%] translate-y-[-50%] w-[260px] text-center'>
                            Number Of Deaths
                            <h5>
                                {
                                    world.map(item => {
                                        return <span key={item} className='text-[#D75F24] text-4xl'>
                                            {item.TotalDeaths}
                                        </span>
                                    })
                                }
                            </h5>
                        </div>
                    </div>
                    <div className=' bg-[#E9E8EE] w-[310px] h-[130px] relative rounded-[20px]'>
                        <div className=' absolute top-[50%] left-[50%] translate-x-[-50%] translate-y-[-50%] w-[260px] text-center'>
                            Total Number Of Restores
                            <h5>
                                {
                                    world.map(item => {
                                        return <span key={item} className='text-[#7BB224] text-4xl'>
                                            {item.TotalRecovered}
                                        </span>
                                    })
                                }
                            </h5>
                        </div>
                    </div>
                </Box>

            </div>
        </section>
    )
}
