import axios from 'axios';
import moment from 'moment';


const headers = {
    'X-RapidAPI-Key': '124d5f6cd1mshf0f4e4913aa0baep171bc2jsn36c019071960',
    'X-RapidAPI-Host': 'vaccovid-coronavirus-vaccine-and-treatment-tracker.p.rapidapi.com'

}
// World Data API : https://rapidapi.com/vaccovidlive-vaccovidlive-default/api/vaccovid-coronavirus-vaccine-and-treatment-tracker/
export const getWorldData = () =>
    axios.get(
        `https://vaccovid-coronavirus-vaccine-and-treatment-tracker.p.rapidapi.com/api/npm-covid-data/world`,
        { headers }
    );
// Countries Data API :  https://documenter.getpostman.com/view/10808728/SzS8rjbc
export const getCountries = () => axios.get('https://api.covid19api.com/countries');
export const getReportByCountry = (country) =>
    axios.get(
        `https://api.covid19api.com/dayone/country/${country}?${moment()
            .utc(0)
            .format()}`
    );
export const getCountries2 = () =>
    axios.get(
        'https://gist.githubusercontent.com/bensquire/1ba2037079b69e38bb0d6aea4c4a0229/raw/8609a1a86683bbd6d0e4a7e9456eabf6e7b65b7f/countries.json',

    );
export const getReportByCountry2 = (country) =>
    axios.get(
        `https://vaccovid-coronavirus-vaccine-and-treatment-tracker.p.rapidapi.com/api/api-covid-data/reports/${country}?${moment()
        .utc(0)
        .format()}`,
        { headers }
    );






















// Get All Coronavirus News :
export const getCoronaVirusNews = () =>
    axios.get(
        'https://vaccovid-coronavirus-vaccine-and-treatment-tracker.p.rapidapi.com/api/news/get-coronavirus-news/0',
        { headers }
    );

//Get All Health News
export const getHealthNews = () =>
    axios.get(
        'https://vaccovid-coronavirus-vaccine-and-treatment-tracker.p.rapidapi.com/api/news/get-health-news/1',
        { headers }
    );

// Get All Vaccine News 

export const getVaccineNews = () =>
    axios.get(
        'https://vaccovid-coronavirus-vaccine-and-treatment-tracker.p.rapidapi.com/api/news/get-health-news/1',
        { headers }
    );


