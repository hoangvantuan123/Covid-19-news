import React from 'react';
import NewsCoronaVirus from './newsCoronaVirus';
import NewsHealth from './newsHealth';
import NewsVaccines from './newsVaccines';

export default function News() {

    return (
        <div className=' '>
            <div className=' mb-6'>
                <h2 className=' font-inria font-semibold text-4xl text-[#3E3E3E]' >
                    Latest Articles
                </h2>
            </div>
            <NewsCoronaVirus />
            <NewsVaccines />
            <NewsHealth />
        </div>
    )
}




