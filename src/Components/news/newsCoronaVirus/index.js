import React, { useEffect } from 'react'
import { getCoronaVirusNews } from '../../API'
import usePagination from '../Pagination'

export default function NewsCoronaVirus() {
    const [data, setNews] = React.useState([]);

    // APi get all coronavirusnews 
    useEffect(() => {
        getCoronaVirusNews().then((response) => {
            //console.log({ response });
            setNews(response.data.news)

        });

    }, []);

    const PER_PAGE = 9;
    // Xử lý Pagination : Phân trang
    //const count = Math.ceil(data.length / PER_PAGE);
    const DATA = usePagination(data, PER_PAGE)
    //console.log('te', DATA) 
    /* const handleChange = (e, p) => {
        setNews(p);
        DATA.jump(p);
    };
 */

    return (
        <div className=' '>
            <div className=' '>
                <div>
                    <h3 className=' font-inter text-2xl font-bold text-[#3E3E3E]'>
                        News CoronaVirus
                    </h3>
                </div>
                <div className=' grid md:grid-cols-1 md:max-w-2xl lg:max-w-[100%]  lg:grid-cols-3  lg:gap-5'>
                    {
                        DATA.currentData().map((item, use) => {
                            return (
                                <div key={use} className='relative  w-[400px] h-[400px]   ' >
                                    <a href={item.link}>
                                        <div className='w-[400px] h-[360px] rounded-3xl' >
                                            <img src={item.urlToImage} alt="" className='rounded-2xl w-[400px] h-[360px] object-cover' />
                                        </div>
                                        <div className='title bg-[#F1F1F2] w-[380px] h-[165px] absolute top-[75%] left-[47.5%] translate-x-[-50%] translate-y-[-50%] p-10'>
                                            <h5>
                                                {item.reference}
                                            </h5>
                                            <h4>
                                                {item.title}
                                            </h4>
                                        </div>
                                    </a>
                                </div>
                            )
                        })
                    }
                </div>
            </div>


        </div>
    )
}
