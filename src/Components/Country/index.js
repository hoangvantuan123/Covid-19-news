import React, { useEffect } from 'react';
import { sortBy } from 'lodash';
import { getCountries, getReportByCountry, } from '../API';
import CountrySelect from './countrySelect';
import CircleMap from './cricleMap';


export default function Country() {

  const [countries, setCountries] = React.useState([]);
  const [selectedCountryId, setSelectedCountryId] = React.useState('');
  const [report, setReport] = React.useState([]);

  useEffect(() => {
    getCountries().then((res) => {
      console.log(res)
      const { data } = res;
      const countries = sortBy(data, 'Country');
      setCountries(countries);
      setSelectedCountryId('vn');

    });
  }, []);

  const handleOnChange = React.useCallback((e) => {
    setSelectedCountryId(e.target.value);

  }, []);



  /*  console.log('tiel',selectedCountryId);  */

  useEffect(() => {
    if (selectedCountryId) {
      const selectedCountry = countries.find(
        (country) => country.ISO2 === selectedCountryId.toUpperCase()
      );
      console.log('test1',selectedCountry);
      getReportByCountry(selectedCountry.Slug).then((res) => {
        /*  console.log('getReportByCountry', { res }); */
        // remove last item = current date
        res.data.pop();
        setReport(res.data);
      });

    }
  }, [selectedCountryId, countries]);

  return (
    <div className=' min-h-[800px]  m-auto '>
      <div className=' mb-6'>
        <h2 className=' font-inria font-semibold text-4xl text-[#3E3E3E]' >
          Update News
        </h2>

        <div>
          <CountrySelect handleOnChange={handleOnChange}
            countries={countries}
            value={selectedCountryId} />
          <CircleMap report={report} />
        </div>
      </div>
    </div>
  )
}
