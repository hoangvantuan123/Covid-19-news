import React, { useMemo } from 'react'

function CircleMap({ report }) {
    const boxitem = useMemo(() => {
        if (report && report.length) {
            const latestData = report[report.length - 1];
            return [
                {
                    count1: latestData.Confirmed,
                    type: 'confirmed',
                },
                {
                    count2: latestData.Recovered,
                    type: 'recovered',
                },
                {
                    count3: latestData.Deaths,
                    type: 'death',
                },
            ];
        }
        return [];
    }, [report]);

    return (
        <div className=' font-inter'>
            <div className=' w-72  h-72 rounded-full  bg-regal-blue  flex justify-center items-center text-center'>
                <h5 className=' '>
                    Total number of infections
                    <br />
                    {
                        boxitem.map((data, id) =>
                        (
                            <span key={id}>
                                {data.count1}
                            </span>
                        ))

                    }
                </h5>
            </div>
            <div className=' w-60  h-60 rounded-full  bg-regal-oranges  flex justify-center items-center text-center'>
                <h5 className=' '>
                    Number of deaths
                    <br />
                    {
                        boxitem.map((data, id) =>
                        (
                            <span key={id}>
                                {data.count3}
                            </span>
                        ))

                    }
                </h5>
            </div>
            <div className=' w-52  h-52 rounded-full  bg-regal-green  flex justify-center items-center text-center'>
                <h5 className=' '>
                    Number of restores
                    <br />
                    {
                        boxitem.map((data, id) =>
                        (
                            <span key={id}>
                                {data.count2}
                            </span>
                        ))

                    }
                </h5>
            </div>

        </div>
    )
}

export default CircleMap
