/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend: {
      fontFamily: {
        inter: ["Inter", "sans-serif"],
        inria: ["Inria Serif","serif"]
       }
    },
    colors:{
      'tahiti': {
        100: '#3C4E66',
        200: 'rgba(60, 78, 102, 0.29);',
      },
      'regal-blue': 'rgba(40, 109, 168, 0.1);',
      'regal-oranges': 'rgba(215, 95, 36, 0.1);',
      'regal-green': 'rgba(123, 178, 36, 0.1);',
      
    }
  },
  plugins: [],
}
